<?php

    $frase = "Tengo muchas ganas de empezar el Bootcamp";
    
    $letras = str_split(strtolower($frase));

    $a = 0;
    $e = 0;
    $i = 0;
    $o = 0;
    $u = 0;

    foreach($letras as $valor) {
        if ($valor == "a") {
            $a++;
        } else if ($valor == "e") {
            $e++;
        } else if ($valor == "i") {
            $i++;
        } else if ($valor == "o") {
            $o++;
        } else if ($valor == "u") {
            $u++;
        }
    }
    
    if ($a > 0 && $e > 0 && $i > 0 && $o > 0 && $u > 0) {
        echo "La frase contiene las 5 vocales";
    } else {
        echo "La frase no contiene todas las vocales";
    }

?>