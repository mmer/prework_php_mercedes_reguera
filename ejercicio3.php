<?php

    $libro = file_get_contents('./el_quijote.txt');

    $libroMin = strtolower($libro);

    $palabras = explode(" ", $libroMin);

    $i = 0;

    foreach ($palabras as $valor) {
        if ($valor == "molino" || $valor == "molino." || $valor == "molino!" || $valor == "molino," || $valor == "molino;") {
            $i++;
        }
    }

    echo "El Quijote tiene $i veces la palabra molino";
?>