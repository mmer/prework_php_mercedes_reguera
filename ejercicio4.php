<?php

   // $ruta = './el_quijote.txt';
   // $nuevaRuta = './el_quijote-nuevo.txt';
   // $palabra = 'Sancho';
   // $nuevaPalabra = 'Morty';

    function reemplazar ($ruta, $nuevaRuta, $palabra, $nuevaPalabra) {
        $libro = file_get_contents($ruta);

        $palabras = explode(" ", $libro);

        foreach ($palabras as $valor) {
            if ($valor == $palabra) {
            $palabras[$valor] = $nuevaPalabra;
        }

        $palabrasEspacios = implode(" ",$palabras);
    
        file_put_contents($nuevaRuta, $palabrasEspacios);
        }
    }

    reemplazar ('./el_quijote.txt', './el_quijote-nuevo.txt','Sancho', 'Morty');
    $nuevoLibro = file_get_contents($nuevaRuta);

    echo $nuevoLibro;

?>